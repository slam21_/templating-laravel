<!DOCTYPE html>
<html>
<head>
	<title>Buat Account Baru</title>
</head>
<body>
	<div>
		<h2>BuatAccount Baru!</h2>
	</div>

	<div> 
		<h4>Sign Up Form</h4>
		<form action="/welcome" method="POST">
			@csrf
			<label for="firstname">First name</label> <br><br>
			<input type="text" name="first_name"> <br><br>
			<label for="lastname">Last name</label> <br> <br>
			<input type="text" name="last_name">
			<br><br>

			Gender <br><br>
				<input type="radio" id="male" name="Gender"><label for="male">Male</label><br>
				<input type="radio" id="female" name="Gender"><label for="female">Female</label>
				<br>
				<input type="radio" id="other" name="Gender"><label for="wanita">Other</label>
				<br><br>
			

			Nationally: <br><br>
				<select>
					<option>Indonesian</option>
					<option>Singapore</option>
					<option>Malaysia</option>
				</select><br><br>
			 

			Language Spoken: <br><br>
				<input type="checkbox" id="bahasaindonesia" name="bahasaindonesia"><label for="menyanyi">Bahasa Indonesia</label><br>
				<input type="checkbox" id="english" name="english"><label for="english">English</label><br>
				<input type="checkbox" id="other" name="other"><label for="other">Other</label><br><br>


			Bio: <br><br>
			<textarea cols="30" rows="10"></textarea>
			<br>
			
			<input type="submit" name="submit" value="Sign Up">
			<form/>
	</div>
</body>
</html>