@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Bordered Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>            
        @endif
        <a class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Pertanyaan Baru</a>
        <table class="table table-bordered">
            <thead>                  
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse($pertanyaan as $key => $data)
                    <tr>
                        <td> {{ $key + 1}} </td>
                        <td> {{ $data -> judul }} </td>
                        <td> {{ $data -> isi }} </td>
                        <td style=" display:flex;"> 
                            <a href="/pertanyaan/{{$data->id}}" class="btn btn-info btn-sm"> Show</a>
                            <a href="/pertanyaan/{{$data->id}}/edit" class="btn btn-default btn-sm"> Edit</a>
                            <form action="/pertanyaan/{{$data->id}}" method="data">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger bt-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" align="center"> Tidak ada pertanyaan!</td>     
                    </tr>
                @endforelse
        </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection