@extends('adminlte.master')

@section('content')
            <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan!!{{$data->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{$data->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul"  name="judul" value="{{ old('judul', $data->judul)}}" placeholder="Enter judul" required>
                        @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control" rows="3" id="isi"  name="isi" value="{{ old('isi', $data->isi)}}" placeholder="Enter isi" required></textarea>
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <!-- <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"> -->
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
            </div>
@endsection