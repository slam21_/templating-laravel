<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view ('pertanyaan.create');
    }

    public function store(Request $request){
    // dd($request->all());
    $request->validate([
        "judul" => 'required|unique:pertanyaan',
        "isi" => 'required|unique:pertanyaan'
    ]);

    $query = DB::table('pertanyaan')->insert([
        "judul" => $request['judul'],
        "isi" => $request['isi']
    ]);
    return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!!'); 
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan->all());
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
        $data = DB::table('pertanyaan')->where('id',$id)->first();
        //dd($data);
        return view('pertanyaan.show', compact('data'));
    }

    public function edit($id){
        $data = DB::table('pertanyaan')->where('id',$id)->first();
        return view('pertanyaan.edit', compact('data'));
    }

    public function update($id, Request $request){
        $request->validate([
            "judul" => 'required|unique:pertanyaan', 
            "isi" => 'required|unique:pertanyaan'
        ]);
        $query = DB::table('pertanyaan')
        ->where('id',$id)
        ->update([
            'judul'=>$request['judul'], 
            'isi'=>$request['isi']
            ]);
        return redirect('/pertanyaan')->with('success', 'berhasil update');
    }
    public function destroy($id){
        DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('pertanyaan')->with('success', 'berhasil dihapus');
    }
}
